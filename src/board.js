import React from 'react';
import Square from './square.js';

export default class Board extends React.Component {

  createSquare = (i, j) => {
    return (<Square
             value={ this.props.squares[i][j] }
             onClick={() => this.props.onClick(i, j)}
           />);
  }

  render() {

    return (
      <div>
        <div className="status">
          {this.createSquare(0, 0)}
          {this.createSquare(0, 1)}
          {this.createSquare(0, 2)}
        </div>
        <div className="status">
          {this.createSquare(1, 0)}
          {this.createSquare(1, 1)}
          {this.createSquare(1, 2)}
        </div>
        <div className="status">
          {this.createSquare(2, 0)}
          {this.createSquare(2, 1)}
          {this.createSquare(2, 2)}
        </div>
      </div>
    );
  }
}