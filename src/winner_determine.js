
const findWinner = (squares) => {
  return findWinnerByRow(squares) || findWinnerByColumn(squares) || findWinnerByDiagonal(squares)
}

const findWinnerByRow = (squares) => {
  let winner = null

  for(let i = 0; i < squares.length; i++){
    for(let j = 0; j < squares[i].length; j++){
      if(squares[i][j] && !winner){
        winner = squares[i][j]
      } else if (winner) {
        if(winner !== squares[i][j]){
          winner = null
          break;
        }
      } else {
        break;
      }
    }
    if(winner != null)
      break;  
  }
  return winner;
}

const findWinnerByColumn = (squares) => {
  let winner = null

  for(let i = 0; i < squares.length; i++){
    for(let j = 0; j < squares[i].length; j++){
      if(squares[j][i] != null && winner == null){
        winner = squares[j][i]
      } else if (winner != null) {
        if(winner !== squares[j][i]){
          winner = null
          break;
        }
      } else {
        break;
      }
    }
    if(winner != null)
      break;  
  }
  return winner;
}

const findWinnerByDiagonal = (squares) => {
  let winner = null

  for(let i = 0; i < squares.length; i++){
      if(squares[i][i] != null && winner == null){
        winner = squares[i][i]
      } else if (winner != null) {
        if(winner !== squares[i][i]){
          winner = null
          break;
        }
      } else {
        break;
      }
  }
  if (winner != null)
    return winner;

  let j = squares.length-1;

  for(let i = 0; i < squares.length; i++){
    if(squares[i][j] != null && winner == null){
      winner = squares[i][j]
    } else if (winner != null) {
      if(winner !== squares[i][j]){
        winner = null
        break;
      }
    } else {
      break;
    }
    j--;
  }
  return winner;

}

export default findWinner
