import React from 'react';
import findWinner from './winner_determine.js';
import Board from './board.js';
import History from './history.js';

export default class Game extends React.Component {

  state = {
    player: 'X',
    history: [{
      squares: Array(3).fill(Array(3).fill(null))
    }],
    step: 0
  }

  changePlayer = (step) => {
    const player = step % 2 == 0 ? 'X' : '0'
    this.setState({
      player
    })
  }

  handleClick = (i, j) => {
    const { history } = this.state
    const currentSquares = history[history.length - 1].squares

    if(!currentSquares[i][j] && !findWinner(currentSquares)){
      const squares = currentSquares.map((s) => s.slice())
      const step = this.state.step + 1

      squares[i][j] = this.state.player

      this.setState({
        history: history.concat({
          squares
        }),
        step
      })

      this.changePlayer(step)
    }
  }

  jumpTo = (step) => {
    const history = this.state.history

    this.changePlayer(step)
    this.setState({ step })
    this.setState({ history: history.slice(0, step + 1) })
  }

  render() {
    const { history } = this.state
    const currentSquares = history[history.length-1].squares
    const winner = findWinner(currentSquares)
    const status = "Next player: " + this.state.player;
    const winnerText = "The winner is: " + winner;

    return (
      <div className="game">
        <div className="game-board">
          <Board 
            squares = { currentSquares }
            onClick = { (i, j) => this.handleClick(i, j) }
          />
        </div>
        <div className="game-info">
          <div>{"Current step: " + this.state.step}</div>
          <div>{winner ? winnerText : status}</div>
          <History
            history = { history }
            onClick = { (step) => this.jumpTo(step) }
          />
        </div>
      </div>
    );
  }
}