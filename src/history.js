import React from 'react';

export default class History extends React.Component {

  historyList = () => {
    return this.props.history.map((player, step) => {
      let text = step > 0 ? "To step: " + step : 'To start'

      return (
        <li key={ step }>
          <button onClick={() => this.props.onClick(step)}> { text } </button>
        </li>
      )
    })
  }

  render() {
    return (
      <ul>{ this.historyList() }</ul>
    )
  }
}